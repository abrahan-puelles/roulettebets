﻿using System;
using roulette.bets.Infrastructure.Interfaces;
using roulette.bets.Model;
using Microsoft.AspNetCore.Mvc;

namespace roulette.bets.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteController : ControllerBase
    {
        IRouletteService _rouletteService;
        public RouletteController(IRouletteService rouletteService)
        {
            _rouletteService = rouletteService ?? throw new ArgumentNullException(nameof(rouletteService));
        }

        [HttpGet]
        [Route("getAll")]
        public IActionResult GetAll()
        {
            return Ok(_rouletteService.GetAll());
        }
        [HttpPost]
        [Route("new")]
        public IActionResult NewRoulette()
        {
            return Ok(_rouletteService.Create());
        }
        [HttpPut("open")]
        public IActionResult OpenRoulette([FromBody] OpenRoulette request)
        {
            try
            {
                _rouletteService.Open(request);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("close")]
        public IActionResult Close([FromBody] CloseRoulette request)
        {
            try
            {
                Roulette roulette = _rouletteService.Close(request);
                return Ok(roulette);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("bet")]
        public IActionResult Bet([FromBody] BetRequest request, [FromHeader(Name = "user-id")] string userId)
        {
            try
            {
                Bet bet = new Bet
                {
                    UserId = userId,
                    IsColor = request.IsColor,
                    Number = request.Number,
                    RouletteId = request.RouletteId,
                    Amount = request.Amount
                };
                _rouletteService.Bet(bet);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
