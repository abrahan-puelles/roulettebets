﻿using roulette.common;
using System;
using System.Collections.Generic;

namespace roulette.bets.Model
{
    public class Roulette
    {
        public Roulette()
        {
            Init();
        }
        private void Init()
        {
            ListRouletteBetDetail = new List<RouletteBetDetail>();
            for (int i = Constants.MIN_NUMBER_BET; i < Constants.MAX_NUMBER_BET; i++)
            {
                ListRouletteBetDetail.Add(new RouletteBetDetail()
                {
                    Number = i,
                    Players = new List<Player>(),
                    IsWinner = false
                });
            }
        }
        public string Id { get; set; }
        public bool Open { get; set; } = false;
        public DateTime? OpeningDate { get; set; }
        public DateTime? ClosingDate { get; set; }
        public List<RouletteBetDetail> ListRouletteBetDetail { get; set; }
    }
}
