﻿namespace roulette.bets.Model
{
    public class Player
    {
        public string UserId { get; set; }
        public double? AmountBet { get; set; }
        public double? PrizeAmount { get; set; }
        public bool IsColor { get; set; }
    }
}
