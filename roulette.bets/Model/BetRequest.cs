﻿namespace roulette.bets.Model
{
    public class BetRequest
    {
        public string RouletteId { get; set; }
        public int Number { get; set; }
        public double Amount { get; set; }
        public bool IsColor { get; set; }
    }
}
