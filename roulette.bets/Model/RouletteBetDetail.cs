﻿using System.Collections.Generic;

namespace roulette.bets.Model
{
    public class RouletteBetDetail
    {
        public int Number { get; set; }
        public List<Player> Players { get; set; }
        public bool IsWinner { get; set; }
    }
}
