﻿using roulette.bets.Infrastructure.Exceptions;
using roulette.bets.Infrastructure.Interfaces;
using roulette.bets.Infrastructure.Repository;
using roulette.bets.Model;
using roulette.common;
using System;
using System.Collections.Generic;
using System.Linq;
using static roulette.bets.Infrastructure.Exceptions.RouletteException;

namespace roulette.bets.Infrastructure.Services
{
    public class RouletteService : IRouletteService
    {
        private IRouletteRepository _rouletteRepository;
        private IDateFormat _dateFormat;
        public RouletteService(IRouletteRepository rouletteRepository, IDateFormat dateFormat) {
            _rouletteRepository = rouletteRepository ?? throw new ArgumentNullException(nameof(rouletteRepository));
            _dateFormat = dateFormat ?? throw new ArgumentNullException(nameof(dateFormat));
        }
        public Roulette Bet(Bet model)
        {
            if (model == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_05,
                    Message = Constants.MESSAGE_ERROR_05
                });
            }
            if (model.RouletteId.ToString().Equals(""))
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_06,
                    Message = Constants.MESSAGE_ERROR_06
                });
            }
            if (model.Amount < Constants.MIN_AMOUNT_BET || model.Amount > Constants.MAX_AMOUNT_BET)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_04,
                    Message = Constants.MESSAGE_ERROR_04
                });
            }

            if (model.Number < Constants.MIN_NUMBER_BET || model.Number > Constants.MAX_NUMBER_BET)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_07,
                    Message = Constants.MESSAGE_ERROR_07
                });
            }

            Roulette roulette = _rouletteRepository.GetById(new SearchRoulette() { Id = model.RouletteId });
            if (roulette == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_001,
                    Message = Constants.MESSAGE_ERROR_01
                });
            }
            if (!roulette.Open)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_03,
                    Message = Constants.MESSAGE_ERROR_03
                });
            }
            var item = roulette.ListRouletteBetDetail.Where(x => x.Number == model.Number).FirstOrDefault();
            if (item.Players.Where(x => x.UserId == model.UserId).FirstOrDefault() == null)
            {
                item.Players.Add(new Player() { IsColor = model.IsColor, AmountBet = model.Amount, UserId = model.UserId });
            }

            return _rouletteRepository.Save(roulette);
        }
        public string Create()
        {
            Roulette roulette = new Roulette()
            {
                Id = Guid.NewGuid().ToString(),
                Open = false,
                OpeningDate = null,
                ClosingDate = null
            };
            var execute = _rouletteRepository.Save(roulette);
            
            return execute.Id;
        }
        public Roulette Search(SearchRoulette model)
        {
            if (model == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_05,
                    Message = Constants.MESSAGE_ERROR_05
                });
            }
            if (model.Id.ToString().Equals(""))
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_06,
                    Message = Constants.MESSAGE_ERROR_06
                });
            }

            return _rouletteRepository.GetById(model);
        }
        public List<Roulette> GetAll()
        {
            return _rouletteRepository.GetAll();
        }
        public Roulette Open(OpenRoulette model)
        {
            if (model == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_05,
                    Message = Constants.MESSAGE_ERROR_05
                });
            }
            if (model.Id.ToString().Equals(""))
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_06,
                    Message = Constants.MESSAGE_ERROR_06
                });
            }
            Roulette roulette = _rouletteRepository.GetById(new SearchRoulette() { Id = model.Id });
            if (roulette == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_001,
                    Message = Constants.MESSAGE_ERROR_01
                });
            }
            if (roulette.Open)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_02,
                    Message = Constants.MESSAGE_ERROR_02
                });
            }
            roulette.Open = true;
            roulette.OpeningDate = _dateFormat.GetDate();

            return _rouletteRepository.Save(roulette);
        }
        public Roulette Close(CloseRoulette model)
        {
            Random randon = new Random();
            var winner = 0;
            bool winnerRandom = false;
            if (model == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_05,
                    Message = Constants.MESSAGE_ERROR_05
                });
            }
            if (model.Id.ToString().Equals(""))
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_06,
                    Message = Constants.MESSAGE_ERROR_06
                });
            }
            Roulette roulette = _rouletteRepository.GetById(new SearchRoulette() { Id = model.Id });
            if (roulette == null)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_001,
                    Message = Constants.MESSAGE_ERROR_01
                });
            }
            if (!roulette.Open)
            {
                throw new RouletteException(400, new ServiceResponse
                {
                    Code = Constants.CODE_ERROR_03,
                    Message = Constants.MESSAGE_ERROR_03
                });
            }
            while (!winnerRandom)
            {
                winner = randon.Next(Constants.MIN_NUMBER_BET, Constants.MAX_NUMBER_BET);
                if (roulette.ListRouletteBetDetail.Where(x => x.Number == winner).Count() > 0)
                {
                    winnerRandom = true;
                }
            }
            foreach (var item in roulette.ListRouletteBetDetail)
            {
                if (item.Number == winner)
                {
                    item.IsWinner = true;
                    foreach (var play in item.Players)
                    {
                        if (play.IsColor)
                        {
                            play.PrizeAmount = play.AmountBet * Constants.COLOR_BET_PAYAOUT;
                        }
                        else
                        {
                            play.PrizeAmount = play.AmountBet * Constants.NUMBER_BET_PAYAOUT;
                        }
                    }
                }
            }
            roulette.ClosingDate = _dateFormat.GetDate();
            roulette.Open = false;

            return _rouletteRepository.Save(roulette);
        }
    }
}
