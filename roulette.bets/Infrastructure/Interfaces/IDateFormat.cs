﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace roulette.bets.Infrastructure.Interfaces
{
    public interface IDateFormat
    {
        DateTime GetDate();
    }
}
