﻿using roulette.common.GenericClass;
using System.Threading.Tasks;

namespace roulette.bets.Infrastructure.Interfaces
{
    public interface ILogRegisterService
    {
        Task<bool> RegisterLog(RegisterModel model);
    }
}
