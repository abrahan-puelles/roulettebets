﻿namespace roulette.bets.Infrastructure.Exceptions
{
    public class ServiceResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
