﻿namespace roulette.bets.Infrastructure.Exceptions
{
    public class JsonErrorResponse
    {
        public string[] Messages { get; set; }
        public object DeveloperMessage { get; set; }
    }
}
