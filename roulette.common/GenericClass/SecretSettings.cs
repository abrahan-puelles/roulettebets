﻿namespace roulette.common.GenericClass
{
    public class SecretSettings
    {
        public string RegionEndpoint { get; set; }
        public string LogGroupName { get; set; }
    }
}
