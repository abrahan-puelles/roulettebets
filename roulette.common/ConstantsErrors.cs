﻿namespace roulette.common
{
    public partial class Constants
    {
        public const int CODE_ERROR_001 = 1;
        public const string MESSAGE_ERROR_01 = "La ruleta no existe";
        public const int CODE_ERROR_02 = 2;
        public const string MESSAGE_ERROR_02 = "La ruleta se encuentra abierta";
        public const int CODE_ERROR_03 = 3;
        public const string MESSAGE_ERROR_03 = "La ruleta se encuentra cerrada";
        public const int CODE_ERROR_04 = 4;
        public const string MESSAGE_ERROR_04 = "El monto ingresado no esta permitido en la apuesta";
        public const int CODE_ERROR_05 = 5;
        public const string MESSAGE_ERROR_05 = "Error en el modelo enviado";
        public const int CODE_ERROR_06 = 6;
        public const string MESSAGE_ERROR_06 = "Debe ingresar una código ruleta";
        public const int CODE_ERROR_07 = 7;
        public const string MESSAGE_ERROR_07 = "El número ingresado no esta permitido en la apuesta";
    }
}
