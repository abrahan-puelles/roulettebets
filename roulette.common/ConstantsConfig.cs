﻿namespace roulette.common
{
    public partial class Constants
    {
        public const string REDIS_KEY_ROULETTE = "REDIS_KEY_ROULETTE";
        public const double NUMBER_BET_PAYAOUT = 5;
        public const double COLOR_BET_PAYAOUT = 1.8;
        public const int MIN_AMOUNT_BET = 0;
        public const int MAX_AMOUNT_BET = 10000;
        public const int MIN_NUMBER_BET = 0;
        public const int MAX_NUMBER_BET = 36;
    }
}
